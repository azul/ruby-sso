ruby-sso - Ruby bindings for ai's libsso
========================================

This is a simple wrapper around "ai sso"(https://git.autistici.org/ai/sso).

For now it is meant to be used in to integrate with the bonafide server to
sign sso tickets to be consumed by soledad server.

It could also be used in an omniauth_aisso strategy to use the tickets to
authenticate to omniauth enabled rails apps such as gitlab or helpy.

Architecture
------------

Following the python bindings closely.

FFI specific objects should be wrapped. Only expose our own classes.


Dependencies
------------

* libsso - obviously
* ffi - for the bindings
