 class DummyTicket
    extend FFI::Library
    ffi_lib 'sso'
    attach_function :sso_ticket_new, [:string, :string, :string, :pointer, :int], :pointer
    attach_function :sso_ticket_free, [:pointer] , :void

    attr_reader :_tkt

    def initialize
      @_tkt ||= DummyTicket.sso_ticket_new 'user',
        'service',
        'domain',
        _dummy_groups,
        3600
    end

    def _dummy_groups
      FFI::MemoryPointer.new :pointer
    end
  end
