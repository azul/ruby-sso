require 'test_helper'
require 'sso/ticket'

class SSO::TicketTest < Minitest::Test

  def test_empty
    assert empty_ticket.empty?
    refute ticket.empty?
  end

  def test_user
    assert_equal 'user', ticket.user
  end

  def test_service
    assert_equal 'service', ticket.service
  end

  def test_domain
    assert_equal 'domain', ticket.domain
  end

  def test_expires
    assert_in_delta ticket.expires, (Time.now.to_i + 3600), 2
  end

  def ticket
    SSO::Ticket.new 'user', 'service', 'domain', [:a, :b]
  end

  def empty_ticket
    SSO::Ticket.new nil, nil, nil, nil
  end
end
