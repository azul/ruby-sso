require 'test_helper'
require 'sso/verifier'

class SSO::VerifierTest < Minitest::Test

  def test_verify
    assert verifier.verify(signed_ticket)
  end

  def verifier
    SSO::Verifier.new public_key
  end

  def signed_ticket
    '4bHHseETK5U9YblImiqUpPHnEktAHIlICzb8w6jfrcrDyj/y7EtWoFVTvmTPcpJKHdh7TPPYgEVHVFH4DwKsCDN8YWxlfHNlcnZpY2UvfHNzby5uZXR8MTQxNTU3NDg0NHw='
  end

  def public_key
    "\xc0\xda\xdb\xb4\x83v[\x05]O\x9f\xf5UM\x92\xb3\xedzC?\x15\xf4\xd8\xeb\xab\xbb\xd0rQ\x0b\xfe#"
  end
end
