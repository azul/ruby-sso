require 'test_helper'
require 'sso/signer'
require 'support/dummy_ticket'
require 'base64'

class SSO::SignerTest < Minitest::Test

  def test_sign
    signature = signer.sign(ticket)
    refute_equal '', signature
    decoded = Base64.decode64 signature
    assert_includes decoded, 'user|service|domain'
  end

  def signer
    SSO::Signer.new '123456'
  end

  def ticket
    @ticket ||= DummyTicket.new
  end

  # Test underlying FFI primitives
  #
  # Not sure what size does.
  # Let's make sure this works.
  def test_mem_pointer_size
    out = FFI::MemoryPointer.new :char, 512
    assert_equal 512, out.size
  end
end
