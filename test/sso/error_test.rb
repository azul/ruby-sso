require 'test_helper'
require 'sso/error'

class SSO::ErrorTest < Minitest::Test

  def test_error_from_error_code
    err = SSO::Error.new -5
    assert_equal "signature verification failure", err.to_s
  end
end
