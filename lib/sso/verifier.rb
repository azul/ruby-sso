require 'ffi'
require 'sso/cast/ticket'

module SSO
  class Verifier
    extend FFI::Library
    ffi_lib 'sso'

    attach_function :sso_ticket_open, [:pointer, :string, :string] , :int

    def initialize(public_key)
      @public_key = public_key
    end

    def verify(ticket_string)
      out = FFI::MemoryPointer.new :pointer
      err_code = sso_ticket_open(out, ticket_string, public_key)
      raise StandardError.new(err_code) if err_code != 0
      ticket = SSO::Cast::Ticket.new out.read_pointer
    end

    protected
    attr_reader :public_key
  end
end
