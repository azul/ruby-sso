require 'ffi'

module SSO
  class Error < RuntimeError
    extend FFI::Library
    ffi_lib 'sso'
    attach_function :sso_strerror, [:int], :string

    def initialize(err_code)
      super sso_strerror(err_code)
    end
  end
end
