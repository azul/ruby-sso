require 'ffi'

module SSO
  module Cast
    extend FFI::Library
    ffi_lib 'sso'

    attach_function :sso_ticket_free, [:pointer] , :void

    class Ticket < FFI::ManagedStruct

      layout :user, :string,
        :service, :string,
        :domain, :string,
        :groups, :pointer,
        :expires, :time_t

      def self.release(ptr)
        SSO::CAST.sso_ticket_free ptr
      end

    end
  end
end

