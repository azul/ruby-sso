require 'ffi'

module SSO
  class Signer
    extend FFI::Library
    ffi_lib 'sso'

    attach_function :sso_ticket_sign, [:pointer, :string, :pointer, :size_t] , :int

    def initialize(sk)
      @sk = sk
    end

    def sign(ticket)
      out = FFI::MemoryPointer.new :char, 512
      err_code = sso_ticket_sign(ticket._tkt, @sk, out, out.size)
      raise StandardError.new(err_code) if err_code != 0
      out.read_string
    end
  end

end
