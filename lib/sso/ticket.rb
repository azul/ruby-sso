require 'ffi'
require 'sso/cast/ticket'

module SSO
  class Ticket
    extend FFI::Library
    ffi_lib 'sso'

    attach_function :sso_ticket_new, [:string, :string, :string, :pointer, :int], :pointer

    attr_reader :_tkt

    def initialize(user, service, domain, _groups, ttl=3600)
      #TODO: actually make use of groups
      groups = FFI::MemoryPointer.new :pointer
      ticket_pointer = self.class.sso_ticket_new(user, service, domain, groups, ttl)
      @ticket_struct = SSO::Cast::Ticket.new ticket_pointer
    end

    def ==(other)
      user == other.user &&
        service == other.service &&
        domain == other.domain &&
        groups == other.groups &&
        expires == other.expires
    end

    def empty?
      !(user || domain || service)
    end

    def user
      ticket_struct[:user]
    end

    def service
      ticket_struct[:service]
    end

    def domain
      ticket_struct[:domain]
    end

    def expires
      ticket_struct[:expires]
    end

    protected
    attr_reader :ticket_struct
  end
end
